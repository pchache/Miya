# Miya
Color Scheme for Sublime Text editor.

![Sublime Text](https://img.shields.io/badge/sublime_text-%23575757.svg?style=for-the-badge&logo=sublime-text&logoColor=important) [![GitHub forks](https://img.shields.io/github/forks/diterbus/Miya?color=%23FF9800&logo=%23FF9800&logoColor=%23FF9800&style=for-the-badge)](https://github.com/diterbus/Miya/network) [![GitHub stars](https://img.shields.io/github/stars/diterbus/Miya?color=%23FF9800&logo=%23FF9800&logoColor=%23FF9800&style=for-the-badge)](https://github.com/diterbus/Miya/stargazers) 

## Preview
![](https://i.imgur.com/ojPCb13.png)

## How to install Miya

Clone this repistory `git clone https://github.com/diterbus/Miya/` or click code and download zip. Move Miya file to C:\Users\YourName\AppData\Roaming\Sublime Text 3\Packages create a folder for color schemes and move in color scheme folder. Click preferences > select color scheme > Miya


> Contact me

Discord: diter#0100
